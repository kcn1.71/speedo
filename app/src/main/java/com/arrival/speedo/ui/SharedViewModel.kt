package com.arrival.speedo.ui

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.arrival.speedo.service.SpeedoServiceConnection
import com.arrival.speedo.service.aidl.OnValueChangeListener
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class SharedViewModel @Inject constructor(private val serviceConnection: SpeedoServiceConnection) :
    ViewModel() {

    private val mutableStateValue = MutableStateFlow(0.0)
    val stateValue: StateFlow<Double> = mutableStateValue.asStateFlow()

    private val listener = object : OnValueChangeListener.Stub() {
        override fun onChanged(value: Double) {
            mutableStateValue.value = value
        }
    }

    init {
        serviceConnection.bind()
        viewModelScope.launch {
            serviceConnection.speedoService.filterNotNull().collect {
                it.addValueListener(listener)
            }
        }
    }

    override fun onCleared() {
        viewModelScope.launch {
            serviceConnection.speedoService.filterNotNull().collect {
                it.removeValueListener(listener)
            }
        }
        serviceConnection.unbind()
        super.onCleared()
    }
}