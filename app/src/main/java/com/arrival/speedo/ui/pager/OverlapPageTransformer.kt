package com.arrival.speedo.ui.pager

import android.view.View
import androidx.viewpager2.widget.ViewPager2
import kotlin.math.abs

class OverlapPageTransformer : ViewPager2.PageTransformer {
    override fun transformPage(view: View, position: Float) {
        view.apply {
            val pageWidth = width
            when {
                position < -1 -> {
                    alpha = 0f
                }

                position <= 0 -> {
                    val scaleFactor = (MIN_SCALE + (1 - MIN_SCALE) * (1 - abs(position)))
                    alpha = 1 - position
                    translationX = pageWidth * -position
                    translationZ = 0f
                    scaleX = scaleFactor
                    scaleY = scaleFactor
                }

                position <= 1 -> {
                    alpha = 1f
                    translationX = 0f
                    translationZ = 1f
                    scaleX = 1f
                    scaleY = 1f
                }

                else -> {
                    alpha = 0f
                }
            }
        }
    }

    companion object {
        private const val MIN_SCALE = 0.75f
    }
}