package com.arrival.speedo.ui

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.MotionEvent
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.WindowCompat
import androidx.core.view.WindowInsetsCompat
import androidx.core.view.WindowInsetsControllerCompat
import androidx.viewpager2.widget.ViewPager2
import com.arrival.speedo.databinding.MainActivityBinding
import com.arrival.speedo.ui.fragment.SpeedometerFragment
import com.arrival.speedo.ui.fragment.TachometerFragment
import com.arrival.speedo.ui.pager.MainViewPagerAdapter
import com.arrival.speedo.ui.pager.OverlapPageTransformer
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MainActivity : AppCompatActivity() {

    private lateinit var binding: MainActivityBinding

    private var lastValue: Float = 0f

    @SuppressLint("ClickableViewAccessibility")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = MainActivityBinding.inflate(layoutInflater)
        setContentView(binding.root)

        hideSystemUI()

        binding.pager.apply {
            val fragments = arrayListOf(SpeedometerFragment(), TachometerFragment())

            adapter = MainViewPagerAdapter(fragments, supportFragmentManager, lifecycle)
            orientation = ViewPager2.ORIENTATION_HORIZONTAL
            isUserInputEnabled = false

            setPageTransformer(OverlapPageTransformer())
            setOnTouchListener { _, event ->
                handleOnTouchEvent(event)
            }
        }
    }

    private fun hideSystemUI() {
        supportActionBar?.hide()
        WindowCompat.setDecorFitsSystemWindows(window, false)
        WindowInsetsControllerCompat(window, binding.root).let { controller ->
            controller.hide(WindowInsetsCompat.Type.statusBars() or WindowInsetsCompat.Type.navigationBars())
            controller.systemBarsBehavior =
                WindowInsetsControllerCompat.BEHAVIOR_SHOW_TRANSIENT_BARS_BY_SWIPE
        }
    }

    private fun handleOnTouchEvent(event: MotionEvent): Boolean {

        if (event.pointerCount != 2) return true

        when (event.actionMasked) {
            MotionEvent.ACTION_POINTER_DOWN -> {
                lastValue = event.x
                binding.pager.beginFakeDrag()
            }

            MotionEvent.ACTION_MOVE -> {
                val value = event.x
                val delta = value - lastValue
                binding.pager.fakeDragBy(delta)
                lastValue = value
            }

            MotionEvent.ACTION_POINTER_UP -> {
                binding.pager.endFakeDrag()
            }
        }

        return true
    }
}