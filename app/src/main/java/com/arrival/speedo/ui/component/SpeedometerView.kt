package com.arrival.speedo.ui.component

import android.content.Context
import android.graphics.*
import android.util.AttributeSet
import android.view.View
import androidx.core.content.withStyledAttributes
import com.arrival.speedo.R
import kotlin.math.min

class SpeedometerView @JvmOverloads constructor(
    context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : View(context, attrs, defStyleAttr) {

    private var step: Int = 0
    private var color: Int = 0
    private var margin: Int = 0
    private var use: Int = 0
    private var typefaceName: String? = null
    private var typeface: Typeface

    var maxValue: Int = 0
    var currentValue = 0
        set(value) {
            field = when {
                value < 0 -> 0
                value > maxValue -> maxValue
                else -> value
            }
            invalidate()
        }

    init {
        context.withStyledAttributes(attrs, R.styleable.SpeedometerView) {
            maxValue = getInteger(R.styleable.SpeedometerView_spd_maxValue, 280)
            step = getInteger(R.styleable.SpeedometerView_spd_step, 20)
            color = getResourceId(R.styleable.SpeedometerView_spd_color, R.color.black)
            margin = getInteger(R.styleable.SpeedometerView_spd_margin_numbers, 0)
            use = getInteger(R.styleable.SpeedometerView_spd_use, USE_SPEEDOMETER)
            typefaceName = getString(R.styleable.SpeedometerView_spd_font)
        }

        typeface =
            try {
                Typeface.createFromAsset(context.assets, "font/$typefaceName.otf")
            } catch (e: RuntimeException) {
                Typeface.create(Typeface.SANS_SERIF, Typeface.BOLD)
            }
    }

    private var mainRadius = 0f
    private var strokeWidth = 0f
    private var clickLength = 0f
    private var clickWidth = 0f
    private var widthCenter = 0f
    private var heightCenter = 0f
    private var currentTextSize = 0f
    private var majorTextSize = 0f
    private var minorTextSize = 0f
    private var centralPinSize = 0f

    private var onePointAngle = 0f

    private val boundaries = RectF()
    private val path = Path()
    private val paint = Paint(Paint.ANTI_ALIAS_FLAG).apply {
        color = Color.argb(
            255,
            Color.red(this@SpeedometerView.color),
            Color.green(this@SpeedometerView.color),
            Color.blue(this@SpeedometerView.color)
        )
    }

    override fun onSizeChanged(width: Int, height: Int, oldWidth: Int, oldHeight: Int) {
        widthCenter = (width / 2).toFloat()
        heightCenter = (height / 2).toFloat()
        mainRadius = (min(width, height) / 2.0).toFloat()
        strokeWidth = (mainRadius / 25)
        clickLength = strokeWidth * 2
        clickWidth = strokeWidth / 4
        currentTextSize = mainRadius / 4
        majorTextSize = mainRadius / 7
        minorTextSize = mainRadius / 14
        centralPinSize = mainRadius / 12
        onePointAngle = SWEEP_ANGLE / maxValue

        boundaries.fromCircle(mainRadius)

        path.moveTo(widthCenter - 15, heightCenter - centralPinSize)
        path.lineTo(widthCenter, (heightCenter - mainRadius * 0.6).toFloat())
        path.lineTo(widthCenter + 15, heightCenter - centralPinSize)
        path.lineTo(widthCenter - 15, heightCenter - centralPinSize)
        path.close()
    }

    override fun onDraw(canvas: Canvas?) {
        super.onDraw(canvas)
        canvas?.drawMainArc()
        canvas?.drawCentralPin()
        canvas?.drawCurrent()
        canvas?.drawLabel()
        canvas?.drawNeedle()
        canvas?.drawClicks()
    }

    private fun Canvas.drawMainArc() {
        paint.apply {
            strokeWidth = this@SpeedometerView.strokeWidth
            style = Paint.Style.STROKE
        }
        drawArc(boundaries, START_ANGLE, SWEEP_ANGLE, false, paint)
    }

    private fun RectF.fromCircle(radius: Float) {
        val left = (widthCenter - radius) + strokeWidth * 2
        val top = (heightCenter - radius) + strokeWidth * 2
        val right = (widthCenter + radius) - strokeWidth * 2
        val bottom = (heightCenter + radius) - strokeWidth * 2
        set(left, top, right, bottom)
    }

    private fun Canvas.drawClicks() {

        paint.apply {
            strokeWidth = this@SpeedometerView.strokeWidth
            style = Paint.Style.STROKE
        }

        val count = save()

        rotate(90 + FINISH_ANGLE, widthCenter, heightCenter)
        drawSingleClick()
        drawClickNumber(maxValue, majorTextSize, Typeface.BOLD)

        rotate(FINISH_ANGLE, widthCenter, heightCenter)
        drawSingleClick()
        drawClickNumber(0, majorTextSize, Typeface.BOLD)

        paint.strokeWidth = clickWidth
        rotate(onePointAngle * step, widthCenter, heightCenter)
        var k = 1
        for (i in step until maxValue step step) {

            if (maxValue - i < step) continue

            drawSingleClick()

            if (k < 0) {
                drawClickNumber(i, majorTextSize, Typeface.BOLD)
            } else {
                drawClickNumber(i, minorTextSize, Typeface.NORMAL)
            }

            rotate(onePointAngle * step, widthCenter, heightCenter)
            k *= -1
        }

        restoreToCount(count)
    }

    private fun Canvas.drawSingleClick() =
        drawLine(
            widthCenter,
            (heightCenter - mainRadius + strokeWidth * 1.5).toFloat(),
            widthCenter,
            (heightCenter - mainRadius + strokeWidth * 1.5 + clickLength).toFloat(),
            paint
        )

    private fun Canvas.drawClickNumber(value: Int, textSize: Float, textStyle: Int) {

        val positionX = widthCenter
        val positionY =
            (heightCenter - mainRadius + strokeWidth * 1.5).toFloat() + clickLength + majorTextSize + margin

        paint.apply {
            this.textSize = textSize
            textAlign = Paint.Align.CENTER
            style = Paint.Style.FILL
            typeface = Typeface.create(typeface, textStyle)
        }

        when (use) {
            USE_TACHOMETER -> value / 1000
            else -> value
        }.also {
            drawText(it.toString(), positionX, positionY, paint)
        }
    }

    private fun Canvas.drawCurrent() {
        paint.apply {
            this.textSize = currentTextSize
            textAlign = Paint.Align.CENTER
            style = Paint.Style.FILL
            typeface = this@SpeedometerView.typeface
        }

        when (use) {
            USE_TACHOMETER -> "%.1f".format(currentValue.toFloat() / 1000)
            else -> currentValue.toString()
        }.also {
            drawText(it, widthCenter, heightCenter + mainRadius - mainRadius / 6, paint)
        }

    }

    private fun Canvas.drawLabel() {
        paint.apply {
            this.textSize = majorTextSize
            textAlign = Paint.Align.CENTER
            style = Paint.Style.FILL
            typeface = this@SpeedometerView.typeface
        }

        when (use) {
            USE_TACHOMETER -> {
                val position = (heightCenter + mainRadius * 0.45).toFloat()
                drawText("l/min", widthCenter, position, paint)
                drawText("x1000", widthCenter, position + majorTextSize, paint)
            }
            else -> {
                drawText("km/h", widthCenter, heightCenter + mainRadius / 2, paint)
            }
        }

    }

    private fun Canvas.drawCentralPin() {
        paint.apply {
            strokeWidth = this@SpeedometerView.strokeWidth
            style = Paint.Style.STROKE
        }
        drawCircle(widthCenter, heightCenter, centralPinSize, paint)
    }

    private fun Canvas.drawNeedle() {
        paint.apply {
            style = Paint.Style.FILL
            strokeWidth = 1f
        }

        val count = save()

        rotate(90f + START_ANGLE, widthCenter, heightCenter)
        rotate(onePointAngle * currentValue, widthCenter, heightCenter)
        drawPath(path, paint)

        restoreToCount(count)
    }

    companion object {
        const val START_ANGLE = 120f
        const val FINISH_ANGLE = 60f
        const val SWEEP_ANGLE = 300f

        const val USE_SPEEDOMETER = 0
        const val USE_TACHOMETER = 1
    }
}