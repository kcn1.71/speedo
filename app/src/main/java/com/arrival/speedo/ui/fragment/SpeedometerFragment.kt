package com.arrival.speedo.ui.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.lifecycleScope
import com.arrival.speedo.databinding.SpeedometerFragmentBinding
import com.arrival.speedo.ui.SharedViewModel
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch

class SpeedometerFragment : Fragment() {

    private val viewModel: SharedViewModel by activityViewModels()

    private var _binding: SpeedometerFragmentBinding? = null
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = SpeedometerFragmentBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewLifecycleOwner.lifecycleScope.launch {
            viewModel.stateValue.collect {
                binding.speedometer.currentValue = (it * binding.speedometer.maxValue).toInt()
            }
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}