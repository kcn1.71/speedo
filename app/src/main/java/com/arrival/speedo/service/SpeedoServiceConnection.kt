package com.arrival.speedo.service

import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.content.ServiceConnection
import android.os.IBinder
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import com.arrival.speedo.service.aidl.SpeedoServiceAidl
import dagger.hilt.android.qualifiers.ApplicationContext
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.asStateFlow
import javax.inject.Inject

class SpeedoServiceConnection @Inject constructor(@ApplicationContext private val context: Context) :
    ServiceConnection {

    private var mutableSpeedoService = MutableStateFlow<SpeedoServiceAidl?>(null)
    var speedoService: StateFlow<SpeedoServiceAidl?> = mutableSpeedoService.asStateFlow()

    override fun onServiceConnected(name: ComponentName?, service: IBinder?) {
        Log.e(TAG, "Service connected")
        mutableSpeedoService.value = SpeedoServiceAidl.Stub.asInterface(service)
    }

    override fun onServiceDisconnected(name: ComponentName?) {
        Log.e(TAG, "Service has disconnected")
        mutableSpeedoService.value = null
    }

    fun bind() {
        Intent().apply {
            action = "com.arrival.speedo.service.SPEEDO_SERVICE"
            `package` = "com.arrival.speedo"
        }.also {
            context.bindService(it, this, AppCompatActivity.BIND_AUTO_CREATE)
        }
    }

    fun unbind() {
        context.unbindService(this)
    }

    companion object {
        const val TAG = "SpeedoServiceConnection"
    }
}