package com.arrival.speedo.service

import android.content.Intent
import android.os.IBinder
import androidx.lifecycle.LifecycleService
import androidx.lifecycle.lifecycleScope
import com.arrival.speedo.service.aidl.OnValueChangeListener
import com.arrival.speedo.service.aidl.SpeedoServiceAidl
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.sync.Mutex
import kotlinx.coroutines.sync.withLock
import kotlin.math.sin

class SpeedoService : LifecycleService() {

    private val listeners = mutableMapOf<IBinder, OnValueChangeListener>()

    private val binder = object : SpeedoServiceAidl.Stub() {
        override fun addValueListener(listener: OnValueChangeListener?) {
            listener?.also {
                val binder = it.asBinder()

                synchronized(LISTENERS_LOCK) {
                    if (!listeners.containsKey(binder)) {
                        listeners[binder] = listener
                    }
                }
            }
        }

        override fun removeValueListener(listener: OnValueChangeListener?) {
            listener?.also {
                val binder = it.asBinder()

                runBlocking {
                    listenersMutex.withLock {
                        listeners.remove(binder)
                    }
                }
            }
        }

        fun notifyListeners(value: Double) {
            synchronized(LISTENERS_LOCK) {
                listeners.values.forEach {
                    it.onChanged(value)
                }
            }
        }
    }

    override fun onBind(intent: Intent): IBinder {
        super.onBind(intent)
        calculateValue()
        return binder
    }

    private fun calculateValue() {
        lifecycleScope.launch(Dispatchers.Default) {
            while (true) {
                val x = System.currentTimeMillis()
                val rads = Math.toRadians(x.toDouble())
                val value = (sin(rads / 10) + sin(rads / 20) + sin(rads / 30) + 2.4) / 4.8
                binder.notifyListeners(value)
            }
        }
    }

    companion object {
        private val LISTENERS_LOCK = Object()
        private val listenersMutex = Mutex()
    }

}