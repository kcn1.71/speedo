package com.arrival.speedo

import android.app.Application
import dagger.hilt.android.HiltAndroidApp
import jp.wasabeef.takt.Takt

@HiltAndroidApp
class BaseApplication: Application() {
    override fun onCreate() {
        super.onCreate()
        Takt.stock(this)
    }
}