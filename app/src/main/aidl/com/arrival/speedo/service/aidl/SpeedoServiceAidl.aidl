package com.arrival.speedo.service.aidl;

import com.arrival.speedo.service.aidl.OnValueChangeListener;

interface SpeedoServiceAidl {
    void addValueListener(in OnValueChangeListener listener);
    void removeValueListener(in OnValueChangeListener listener);
}