package com.arrival.speedo.service.aidl;

interface OnValueChangeListener {
    void onChanged(double value);
}